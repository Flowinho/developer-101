# Developer 101

A collaborative list on what i consider the core competences of a software developer.

## Skills

|Skill|Kind|Reason|Scope(?)
|:--|:-:|:--|:--|
|GIT|Tooling|A developer has to know his/her tools, and GIT is at the heart of everything.|A deeper understanding of GIT enables you to troubleshoot and solve problems that arise with source-code-management and CI. [Tutorial](https://www.atlassian.com/de/git/tutorials/learn-git-with-bitbucket-cloud)|
|Ruby on Rails|Language|An easy-to-understand, all-purpise language.|Ruby on Rails is used widely in software development. Learning it takes not long but definitely helps out during every-day-work.|
|Clean Code|Book|The "bible of software-enginners".|Robert C. Martin aka "Uncle Bob" delivers the fundamentals of software-development.|
|Terminal|Tooling|The terminal is the only tool that will always be available to a developer.|Understanding and working with the terminal is crucial. Shells to look into: bash, zshell|
|Shell-Scripts|Tooling|Used by developers to automatise their work.|Basic scripts that help in every-day-work, optional: shell-scripts that define build-jobs.|
|Makefiles|Tooling|Used by developers to automatise their work and / or pipeline-jobs.|Makefiles are present everywhere, so fundamentally understand how they work is a good skill.|
|SSH|Tooling|Basic skill to access the terminal at remote-machins (e.g. continous-deployment)|Connecting, understanding authorisation, using different ports, ssh-tunnel-forwarding.|
|Unit-testing|Code||Writing proper unit-tests, knowing the differences between unit- / system- and integration tests.|
|Markdown|Language|Core way of writing text, this list is written in Markdown.|Understanding everything. Should take 2 hours max.|

## Principles 

|Prinicples|Why|Short description|
|:--|:--|:--|
|S.O.L.I.D.|Fundamental understanding of software-development.|Single responsibility, Open-closed, Liskov substitution, Interface segregration, Dependency inversion|
|GIT - Rebase vs. Merge|Fundamental to collaborative coding.|Both things are valid but solve different things.|
|GIT - Git-flow|Fundamental to collaborative coding.|Easy-to-understand, proven method of branching your SCM.|
|Depdency-inversion|Proven way to include third-party software into your project w/o adding a hard-dependency.|||
|Downsides of OOP||||
|SDLC||Software development lifecycle|
|Hard-coupling|Connecting bits of source-code that don't work w/o each other.||


## Architectural Principles

### Frontend

|Principle|Short description|Why it's relevant|
|:-:|:--|:--|
|MVC|Model-View-Controller||
|MVVM|Model-View-ViewModel||

### Backend

- Micro-service architecture

## Software-development process

- Domain-driven design

### Scrum

- Burndown-chart
- Remaining estimate and why it's important
- Control-chart (Jira)
- User-story
- Retrospective
- Proper way of doing a daily Scrum
- SAFe (scaling Scrum over multiple teams)

## Websites & platforms to understand and master

|Name|Reason|
|:-:|:--|
|[GitHub](https://github.com)|Heart of open-source development.|
|[GitLab](https://gitlab.com)|Free (as in freedom) alternative to GitHub.|
|[Stack Overflow](https://stackoverflow.com/)|The most established forum for all developers.|
|[IRC - Freenode](https://freenode.net/)|Established chat-platform mainly for open-source developers, especially freenode.net|